import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import json
from xlsxwriter import Workbook
import os
import time

keys = {
    'difficulty':'difficulty',
    'time':'time',
    'mX': 'mX',
    'mY': 'mY',
    'autoP':'autoP',
    'rX': 'rX',
    'rY': 'rY',
    'tA': 'tA',
    'tB': 'tB',
    'tC': 'tC',
    'tD': 'tD',
    'tE': 'tE',
    'tF': 'tF',
    'p1': 'p1',
    'p2': 'p2',
    'p3': 'p3',
    'p4': 'p4',
    'p5': 'p5',
    'p6': 'p6',
    'p7': 'p7',
    'p8': 'p8',
}

filename = ''

root = tk.Tk()

canvas1 = tk.Canvas(root, width=300, height=300, bg='lightsteelblue2', relief='raised')
canvas1.pack()

label1 = tk.Label(root, text='File Conversion Tool', bg='lightsteelblue2')
label1.config(font=('helvetica', 20))
canvas1.create_window(150, 60, window=label1)

def create_xlsx_file(file_path: str, headers: dict, items: list):
    with Workbook(file_path) as workbook:
        worksheet = workbook.add_worksheet()
        worksheet.write_row(row=0, col=0, data=headers.values())
        header_keys = list(headers.keys())
        for index, item in enumerate(items):
            row = map(lambda field_id: item.get(field_id, ''), header_keys)
            worksheet.write_row(row=index + 1, col=0, data=row)

def getJSON():
    global filename
    filename = filedialog.askopenfilename(filetypes=[('Json file', '*.json'), ('All files', '*.*')])

def startConvert():
    print('=============== START CONVERT ===================')
    global filename
    if filename == '':
        print('no file')
    else:
        file = os.path.basename(filename)
        file_text = file.split('.json')[0]
        print(file_text)
        with open(filename) as f:
            file = json.load(f)
            difficulty0 = file['difficulty0']
            data0 = file['data0']
            difficulty = file['difficulty']
            data = file['data']
            for row in data0:
                row['difficulty'] = difficulty0
                timestamp = row['time']
                current_time = time.ctime(timestamp/1000)
                row['time'] = current_time
                print(timestamp, current_time)

            for row in data:
                row['difficulty'] = difficulty
                data0.append(row)
                timestamp = row['time']
                current_time = time.ctime(timestamp / 1000)
                row['time'] = current_time


            print(len(data))
            create_xlsx_file('result/'+ file_text + ".xlsx", keys, data0)
    print('=============== ENB CONVERT ===================')

browseButton_CSV = tk.Button(text="      Import JSON File     ", command=getJSON, bg='green', fg='white',
                             font=('helvetica', 12, 'bold'))
canvas1.create_window(150, 130, window=browseButton_CSV)

saveAsButton_Excel = tk.Button(text=' Convert CSV to Excel ', command=lambda *args: startConvert(), bg='green', fg='white',
                               font=('helvetica', 12, 'bold'))
canvas1.create_window(150, 180, window=saveAsButton_Excel)


def exitApplication():
    MsgBox = tk.messagebox.askquestion(' Exit Application ', 'Are you sure you want to exit the application',
                                       icon='warning')
    if MsgBox == 'yes':
        root.destroy()


exitButton = tk.Button(root, text='        Exit Application      ', command=exitApplication, bg='brown', fg='white',
                       font=('helvetica', 12, 'bold'))
canvas1.create_window(150, 230, window=exitButton)

root.mainloop()